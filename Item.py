#Item.py
#Author: Gerard Andrews
#12/13/2016
#CMPT120
#Creates a Item class for a text based game

class Item:
    def __init__(self, name, description): 
        self.name = name
        self.description = description
