#Game Version 1.0
#Author: Gerard Andrews
#CMPT120-113
#Date: 12/15/2016

from Player import Player
from Player import playerCustomization
from Locale import Locale
from Locale import SecuredLocale
from Item import Item

none = -1 
nowhere = -1
entrance = 0 
mainfloor = 1 
basement = 2 
crypt = 3 
floor1 = 4 
floor2 = 5 
treasureroom = 6
dungeon = 7
outskirts = 8
bathroom = 9
provingGrounds = 10
passageWay = 11
templeHeight = 12
item1 = 1
item2 = 3
item3 = 4
item4 = 6
item5 = 9
item6 = 12
startLoc = 0
hintsleft = 1
       
player = Player()
    
dirName = [ "north", "east", "west", "south" ]

items = [
    Item("gem", "Extremely valuable, could be worth a fortune."),                                       #Will be in Temple Height (location 12)
    Item("map", "A well drawn map that appears to show all locations inside and outside the temple."),  #Will be on mainfloor (location 1)
    Item("key", "Could this open a place somewhere?"),                                                  #Will be in outskirts (location 9)
    Item("torch", "Something to light your way through the temple."),                                   #Will be in crypt
    Item("rope", "A very long rope to escape the building if need be"),                                 #Will be in floor1 (location 4)
    Item("sword", "Good for cutting down stuff that's not here!")                                       #Will be in hidden passage (location 11)
        ]

def canEnter():                                             #Checks if the player can enter the secured location
    if not items[2].name in player.pInv:                    #If the player does not have the key in his inventory
            return False                                        
    return True
        
locations = [
    Locale(entrance, "You currently outside the temple the door is unlocked."                                                                       #0
          "Straight ahead is the mainfloor of the temple. \n", none, [mainfloor, nowhere, nowhere, outskirts]),
    Locale(mainfloor, "You are now on the main floor of the temple the right you can head to the first floor."                                      #1
          " To your left there is a staircase that heads downstairs. Perhaps there's something here. \n",
           items[1], [nowhere, floor1, basement, entrance]),
    Locale(basement, "You are now in the basement, filled with cobwebs, broken objects, rusty iron,"
          " and other weird stuff you can head back up by going back right or you can head north,"                                                   #2
          " to what looks like a crypt. \n", none, [crypt, mainfloor, nowhere, nowhere]),
    Locale(crypt, "You head to the upmost part of the basement and discover a door that leads to a crypt."
          " Burial tombs all over the places, surrounded by cobwebs and dust."                                                                       #3
          " there is not much here to look at, perhaps its time to head back up, or perhaps there's something here. \n",
           items[3], [dungeon, nowhere, nowhere, basement]),
    Locale(floor1, "You are now on floor 1, you can head north to the second floor."                                                                 #4
          " Or you can head back down using the starcase to your left. Could there be something here? \n",
           items[4], [floor2, bathroom, mainfloor, nowhere]),
    Locale(floor2, "You are now on the second floor."
          " The treasureroom is nearby and the entrance is in one of the directions in the room."                                                    #5
          " Or you can head back to the first floor. \n", none, [templeHeight, provingGrounds, treasureroom, floor1]),
    SecuredLocale (treasureroom, "You enter through the hidden passageway and realizes where you are." 
          " Treasured ornaments dangle before you."
          " Diamonds, rubys, sapphires, emeralds and opals, glitter before your very eyes."
          " You quickly get your treasure bag out and collect a much as he/she can."                                                                #6
          " After finsihing, you decides that it's time to leave. \n", none, [nowhere, floor2, nowhere, nowhere], items[2]),
    Locale(dungeon, "You head further into the crypt and come across a dungeon,"                                                                    #7
           " there are many corridors and many dangers to be found here, let's see where it leads! \n",
            none, [treasureroom, passageWay, nowhere, crypt]),
    Locale(outskirts, "You are now at the outskirts of the temple that you passed earlier before you go to the entrance."                           #8
           " Could there possibly be something here? \n", items[2], [entrance, nowhere, nowhere, nowhere]),
    Locale(bathroom, "You are in floor 1's bathroom, feel free to rest here as much as you want...at your own"                                          #9
           " caution, don't be the one guy that falls head first into the toilet obviously. \n", none, [nowhere, nowhere, floor1, nowhere]),
    Locale(provingGrounds, "You are now on the proving grounds, feel free to train you martial arts skills if you,"
           " because it won't be helping either way", none, [nowhere, nowhere, floor2, entrance]),                                                     #10
    Locale(passageWay, "This dungeon is full of surprises, this dungeon has a door that leads to another passageway"
            " It's quite the fortunate surprise too, it appears to be an empty room, maybe there's more to it than"
            " you'd expect.  Could there be somehting here? \n", items[5], [nowhere, nowhere, dungeon, nowhere]),                                      #11
    Locale(templeHeight, "You are now at the highest point in the temple, just feel that breeze!"
           "More importantly is the very colorful gem that could possibly be removed. \n", items[0], [nowhere, nowhere, nowhere, floor2])                 #12
           ]
        
def creditDisplay(): #Displays credits & Copyright
    print("© 2016 Gerard Andrews All Rights Reserved, gerard.andrews1@marist.edu \n")
 
def gameIntro():
    title = "HUNTER" #Defines the Title
    print(title, "\n") #Prints the Title of the game
    #Text below is the backstory
    print("Welcome to Hunter, ", player.name, " You are on the hunt of a hidden treasure locked away in a dungeone somwhere. "
           " You didn't know where to look until finally you come across a map buried near your home. ",
           player.name, " follows the map's directions and you are finally at the temple! ",
           player.name, " has prepared for this his/her whole life and finally the opportunity has arrived! "
           "There's no time to waste, Onward! \n")
    input("Press enter to begin...\n")
     
def getDest(dirId, startLoc):
    dest = locations[startLoc].paths[dirId] #The destination is set equal to the starting locations nav matrix
    if (dest) == nowhere:                   #If the destination = -1
        dest = startLoc                     #The destination stays at where the player was when the cmd was input
    elif (dest) == 6:
        if canEnter() == True:
            print("You have the key! You can enter! \n")
        else:
            print("No key, no entry! \n")
            dest = startLoc
    return dest                                             #Returns the destination to move the player to  

def moveTo(dirId, currentLoc):                              #Index for direction and Index for the current Location
    global dirName
    player.currentLoc = getDest(dirId, currentLoc)          #The player's current location is equal to what the getDest function
    player.moveCount += -1                                  #Remove one move from the player's moveCount if the move is successful
    print("You have", player.moveCount, "moves left. \n")   #Print how many moves the player has left
    if not locations[currentLoc].visited:                   #If the Current Location isn't visited
        locations[currentLoc].visited = True                #The visited variable is set to true for that location
        player.score += 5                                   #Player gets five points for changing the variable
    return player.currentLoc                                #The currentLoc is returned
         
def sceneRender():
     print(locations[player.currentLoc].description)   #Displays the scene after input
 
def mapDisplay(): 
     mapLoc = '''
              Dungeon (Only to room not back) Temple Height
                |-----|-----Passage Way       |-----|
                |     |                       |
                |     |---------|             |
              Crypt       Treasure Room----Floor 2 --------Proving Grounds
               |                            |                     | (Only Down)
               |                            |                     |
               |                            |                     |
             Basement-----Mainfloor--------Floor 1----Bathroom    |
                            |                                     |
                         Entrance---------------------------------|
                            |
                            |
                            |
                        Outskirts
             ''' #Displays a text based map
     print(mapLoc)
 
def endDisplay():   
     print(player.name, "has done it!  He/She found the treasure and has left the temple!"
           "The quest is complete! \n")
     
def helpDisplay():
     print("For movement, you are able to move: north, south, east, and west at all locations."
           " You may type examine along with an item name to determine if there is a specific item in this location."
           " You may type take along with the item name to pick up an item and put it in your inventory."
           " You may type drop along with an item name to drop an item"
           " You may type inventory to view what's in your inventory."
           " You may type ask for a one time hint"
           " You type map to display a map of the whole temple."
           " You may type points to display your total score."
           "You may also type quit to leave the game. \n")                                      #Help Command Display Message
     
def itemCheck():
    if len (player.pInv) == len(items):
        return True
    return False
def main():
    global paths, dirName, hintsleft
    
    q = (player.name," gives up and heads back home...loser... \n") #Command that displays a message if the player quits
    #Error Display
    errorCmd = "Either there is nothing in that direction or you typed an invalid command please try again. \n" #Command That Displays an Error
 
    #Initialized Location
    startLoc = 0
    sceneRender()
     #Start Game loop
    while (True):
         # 1. GET AND PROCESS USER INPUT        
        cmd = (input("You are allowed to make a choice of where to go please type a direction, type help if you require help on controls: \n"))     #User input here
        cmd = cmd.lower()                                                                                                                       #Input will automatically be lowercased
        cmd = cmd.split(" ")                                                                                                                    #Command will be split automatically if more than one string
                                                                                                                                                 #Cmd will refer to an array, so that
                                                                                                                                                 # cmd[0] will be the first string, cmd[1] will be the next, etc.
        # 2. UPDATE THE GAME STATE
        if (cmd[0]) == "quit" or (cmd[0]) == "q":                                                                                               #If the cmd = quit or q
            print(q, " \n")                                                                                                                     #Print the quit message and break the loop
            break
        elif (cmd[0]) == "help" or (cmd[0]) == "h":                                                                                             #If cmd = help or h
            helpDisplay()                                                                                                                       #Print the help message and continue
            continue
        elif (cmd[0]) == "map" or (cmd[0]) == "m":                                                                                              #If cmd = map or m
            for i in(player.pInv):                                                                                                              #for loop that checks all items the playerInv
                if i == "map":                                                                                                                  #If the map is in the inventory it will display the map             
                    mapDisplay()
            if player.pInv == []:
                    print("Were you expecting a map to just pop up?!")
                    copyright                                                                                                               #Otherwise it will print a message that informs the player that he can't view the map
            else:
                i = None
                print("It would be great to have a map right now... \n")
                continue
        elif (cmd[0]) == "points" or (cmd[0]) ==  "p":                                                                                           #If cmd = points or p
            print("your current score is:", player.score, "points! \n")                                                                      #Will the display the player's score
            continue
        elif (cmd[0]) == "take" or (cmd[0]) == "t":                                                                                          #If cmd = take or t                                                                                        #The second word from split must = an item name from the items list
            here = locations[player.currentLoc]
            if here.itemLoc == none and len (cmd) < 2:                                                                                                         #If the player's location's itemLoc = none
                print("There's nothing here! \n")                                                                                            #Display a message that say's no item is here
                continue
            elif here.itemLoc == none:
                print("There's nothing here! \n")
            elif here.itemLoc != none and len (cmd) < 2:
                print("Take what? \n")
                continue      
            elif cmd[1] == here.itemLoc.name:                                                                                                # there is an item here, now check to see if that item's name is the same name the player asked to take
                player.pInv.append(cmd[1])
                here.itemLoc = none                                                                                                          #Othe player's inventory will append the item from that location
                print("You now have the", cmd[1], ".\n")                                                                                     #Display a message that shows that the player has the item
                continue
            else:
                print("That item is not in this location or there is just simply no item here. \n")
                continue
        elif (cmd[0]) == "examine" or (cmd[0]) == "x":
            here = locations[player.currentLoc]                                                                                              #If cmd = examine or x, The second word from split must = an item name from the items list
            if here.itemLoc == none  and len (cmd) < 2:                                                                                                         #If the player's location's itemLoc = none
                print("There is no item in this location. \n")
                sceneRender()
                continue
            elif len (cmd) < 2:
                print("Examine what? \n")
                sceneRender()
                continue                                                                                                                                #Display a message that say's no item is here
            elif cmd[1] == items[player.currentLoc].name:
                if cmd[1] == here.itemLoc.name:
                    print("There is a", items[player.currentLoc].name, " on the floor. \n")                                                      #Otherwise state the item that is here and print it's description
                    print(items[player.currentLoc].description)  
                    continue
            else:
                print("either that item is in a different area, or that just simply doesn't exist! \n")
                continue
            
        elif (cmd[0]) == "drop" or (cmd[0]) == "d":                                                                                          #If cmd = drop or d
            here = locations[player.currentLoc]                                                                                              #The second word from split must be an item name from the items list
            for x in (player.pInv):                                                                                                          #A for loop to check each item in the player's inventory                                                                                                                                                                            
                if x == cmd[1]:                                                                                                              #If the item name is in the player's inventory through the player's input
                    player.pInv.remove(x)                                                                                                    #The item will be removed from the player's inventory
                    here.itemLoc = items[player.currentLoc].name                                                                             #The location's itemLocation will now change from none to that item name
                    print("You dropped the", x, " on the floor. \n")                                                                            #Indicate the player dropped the item via messsage
                    continue
                elif len (cmd) < 2:
                    print("Drop what? \n")
                    continue
                else:
                   print("You don't have such an item in your inventory! \n")                                                                #If the item is not in the player's inventory then a message will display that  
                   continue                                                                                                                  #the player doesn't have that item
        elif (cmd[0]) == "ask" or (cmd[0]) == "a":
            if hintsleft > 0:
                print("There must be some sort of assistance outside the temple \n")
                hintsleft -= 1
                continue
            else:
                print("Hope you wrote that hint down because your not getting another one! \n")
                continue
                                                                                                                                        
        elif (cmd[0]) == "inventory" or (cmd[0]) == "i":                                                                                     #If cmd = inventory or i, the program will print out the player's inventory                                       
            print(player.pInv)
            continue
        
        elif (cmd[0]) == "n" or (cmd[0]) == "north":                                                                                         #If cmd = any direction, Call the moveTo function
            player.currentLoc = moveTo(0, player.currentLoc)                                                                                 # pass in the current location AND the direction number
        elif (cmd[0]) == "e" or (cmd[0]) == "east":                                                                                          #The return from the function will move the player
            player.currentLoc = moveTo(1, player.currentLoc)                                                                                 #If it isn't a cmd or leads to nowhere print a error message to prevent crashing
        elif (cmd[0]) == "w" or (cmd[0]) == "west":
            player.currentLoc = moveTo(2, player.currentLoc)
        elif (cmd[0]) == "s" or (cmd[0]) == "south":
            player.currentLoc = moveTo(3, player.currentLoc)
        else:
            print(errorCmd+" \n")
            continue
            
        # 3. RENDER/DISPLAY THE SCENE
        sceneRender() #Updates the player's score & Displays the scene after each input.
        
        if player.moveCount == 0:                                                                                                           #If player runs out of moves then print a game over message and break loop
                 print(player.name," had a heart attack as he/she was traveling and died, the end. \n")
                 break
        elif player.currentLoc == 6 and itemCheck() == True:                                                                                 #If the player reaches treasureroom, then display the scene.
            endDisplay()
            break
        else:
            continue
            
gameIntro()
main()
retry = input("Do you want to play again? (Y/N) \n")
retry = retry.lower()                                                                                                                   #Retry will always be given if the player wins or loses the game
if retry == "y":                                                                                                                             #If player types Y
    gameIntro()                                                                                                                              #Game will reset back to the backstory and the player instance will be reset
    player.currentLoc = 0
    player.score = 0
    player.pInv = []
    player.moveCount = 35
    main()
else:
    retry = "n"
    creditDisplay()                                                                                                                          #Otherwise if player doesn't say Y just print the credit and terminate the program
