#Player.py
#Author: Gerard Andrews
#Notes: Gives a Player class for a text adventure

def playerCustomization():
    #Prompt user to input name
    name = (input("Before we begin the game, please enter your full name: \n"))     #User will enter the player's name
    name = name.title()                                                             #Capitalizes the first letters
    return name       

class Player:
    def __init__(self):
        self.name = playerCustomization()   #instance will run a name returned from the playerCustomization function
        self.pInv = []                      #empty inventory
        self.moveCount = 35 
        self.score = 0
        self.currentLoc = 0

        
