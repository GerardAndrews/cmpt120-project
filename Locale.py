#Locale.py
#Author: Gerard Andrews
#CMPT120
#12/13/2016
#Creates a Locale class and Secured Locale child class

class Locale:
    
    def __init__(self, name, description, itemLoc, paths):
        self.name = name
        self.description = description
        self.itemLoc = itemLoc
        self.visited = False
        self.paths = paths

class SecuredLocale(Locale):
    def __init__(self, name, description, itemLoc, paths, requirement):
        Locale.__init__(self, name, description, itemLoc, paths)
        self.requirement = requirement
